# SerialDeC - Serial Device Communication Tool
- [What is this?](#features)
- [Screenshots](#features)
- [Platforms](#platforms)
## What is this?
This is util application for communication using serial port. Main feature of this application is support for many representations of data, for now is three representations: Text(ASCII), Hexadecimal and Binary. My plan is to extend app of plugins support in Lua to make more flexible for user that want create features on your own.  
## Screenshots
![SerialDeC Screen1](documentation/screenshot1.png)
![SerialDeC Screen2](documentation/screenshot2.png)
## Platforms
    - MacOS
    - Linux (soon)
    - Windows (soon)
    
