#include "StringUtil.hpp"
#include <sstream>

unsigned char *sd::string::to_unsigned_char::convert(const std::string &data) {

	auto result = new unsigned char[data.length() + 1];
	memset(result, 0, data.length() + 1);

	for (std::size_t i = 0; i < data.length(); i++) {
		result[i] = static_cast<unsigned char>(data.at(i));
	}
	return result;
}

std::string sd::character::to_string(char data) {
	return std::string(1, data);
}

dstools::byte_array sd::byte_array::to_unsigned_char::convert(const std::string &p_data) {
	dstools::byte_array result_data;
	result_data.reserve(p_data.length());

	for (char i : p_data) {
		result_data.emplace_back(static_cast<dstools::byte>(i));
	}

	return result_data;
}
