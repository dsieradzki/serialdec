
#ifndef SERIALDEC_STRINGUTIL_HPP
#define SERIALDEC_STRINGUTIL_HPP

#include <string>
#include <cstring>
#include <functional>
#include <vector>
#include <serial/SerialPort.hpp>


namespace sd {
	namespace string {
		namespace to_unsigned_char {
			unsigned char* convert(const std::string &data);
		}
	}
	namespace byte_array {
		namespace to_unsigned_char {
			dstools::byte_array convert(const std::string &data);
		}
	}

	namespace character {
		std::string to_string(char data);
	}

}


#endif //SERIALDEC_STRINGUTIL_HPP
