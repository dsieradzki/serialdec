#ifndef CARWIZARD_SERIALPORTDETECTOR_HPP
#define CARWIZARD_SERIALPORTDETECTOR_HPP

#include <string>
#include <vector>
#include <memory>

namespace ds {
	class SerialPortDetector {
	public:
		virtual ~SerialPortDetector() = default;

		virtual std::vector<std::string> get_list_of_serial_ports() = 0;
	};

}

#endif //CARWIZARD_SERIALPORTDETECTOR_HPP
