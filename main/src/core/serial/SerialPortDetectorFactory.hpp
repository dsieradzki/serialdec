
#ifndef SERIALBOX_SERIALPORTDETECTORFACTORY_HPP
#define SERIALBOX_SERIALPORTDETECTORFACTORY_HPP

#include "SerialPortDetector.hpp"

#if _WIN32
#include "../../../../platform/windows/WindowsSerialPortDetector.hpp"

#elif __APPLE__

#include "../../../../platform/macos/SerialPortDetectorMacos.hpp"

#elif __linux__
#include "../../../../platform/linux/SerialPortDetectorLinux.hpp"
#endif
namespace ds {
	ds::SerialPortDetector *createSerialPortDetector() {
#if _WIN32
		return new ds::WindowsSerialPortDetector();
#elif __APPLE__
		return new ds::SerialPortDetectorMacos();
#elif __linux__
		return new ds::SerialPortDetectorLinux();
#endif
	}
}

#endif //SERIALBOX_SERIALPORTDETECTORFACTORY_HPP
