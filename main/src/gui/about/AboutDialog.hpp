
#ifndef SERIALBOX_ABOUTDIALOG_HPP
#define SERIALBOX_ABOUTDIALOG_HPP


#include <wx/wx.h>

class AboutDialog: public wxDialog {
public:
	explicit AboutDialog(wxWindow *parent);
};


#endif //SERIALBOX_ABOUTDIALOG_HPP
