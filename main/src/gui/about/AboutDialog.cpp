
#include "AboutDialog.hpp"
#include "../../Version.h"

AboutDialog::AboutDialog(wxWindow *parent) : wxDialog(parent, -1, wxT("About"), wxDefaultPosition, wxDefaultSize) {

	auto *main_sizer = new wxBoxSizer(wxVERTICAL);

	this->SetSizer(main_sizer);

	std::string title("SerialDeC ");
	title.append(APPLICATION_VERSION);

	auto *title_label = new wxStaticText(this, wxID_ANY, title);
	wxFont font = title_label->GetFont();
	font.SetPointSize(25);
	font.SetWeight(wxFONTWEIGHT_BOLD);
	title_label->SetFont(font);
	main_sizer->Add(title_label, 0, wxALL, 20);

	auto *row_author_info_sizer = new wxBoxSizer(wxHORIZONTAL);
	auto *author_label = new wxStaticText(this, wxID_ANY, wxT("Author:"));
	auto *author_label_value = new wxStaticText(this, wxID_ANY, wxT("Damian Sieradzki"));
	row_author_info_sizer->Add(author_label);
	row_author_info_sizer->Add(author_label_value);
	main_sizer->Add(row_author_info_sizer, 0, wxLEFT | wxRIGHT, 10);

	auto *row_support_info_sizer = new wxBoxSizer(wxHORIZONTAL);
	auto *support_label = new wxStaticText(this, wxID_ANY, wxT("Support:"));
	auto *support_label_value = new wxStaticText(this, wxID_ANY, wxT("damian.sieradzki@hotmail.com"));
	row_support_info_sizer->Add(support_label);
	row_support_info_sizer->Add(support_label_value);
	main_sizer->Add(row_support_info_sizer, 0, wxLEFT | wxRIGHT, 10);

	auto *close_button = new wxButton(this, wxID_ANY, wxT("Close"));
	close_button->Bind(wxEVT_COMMAND_BUTTON_CLICKED, [=](wxCommandEvent& WXUNUSED(event)) {
		this->EndModal(0);
	});
	main_sizer->Add(close_button, 0, wxTOP | wxRIGHT | wxALIGN_RIGHT, 10);


	auto *copyright_label = new wxStaticText(this, wxID_ANY,
	                                         "Copyright (C) 2019 Damian Sieradzki. All rights reserved.");
	main_sizer->Add(copyright_label, 0, wxALL, 10);


	Centre();
	this->Fit();
	this->SetMinSize(this->GetSize());
	this->SetMaxSize(this->GetSize());
}
