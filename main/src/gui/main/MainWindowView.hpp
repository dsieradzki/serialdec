
#ifndef SERIALBOX_MAINWINDOWVIEW_HPP
#define SERIALBOX_MAINWINDOWVIEW_HPP


#include <wx/wx.h>
#include "representation/view/basic/DataView.hpp"
#include "connection/ConnectionView.hpp"
#include "representation/view/basic/BasicView.hpp"
#include "representation/view/basic/BasicViewPresenter.hpp"
#include "representation/view/advanced/AdvancedView.hpp"

class MainWindowView : public wxFrame {
public:
	explicit MainWindowView(const wxString &title);

	wxMenuBar *m_menubar;
	sd::ConnectionView *m_connection_view;

	BasicView *m_basic_view;
	AdvancedView *m_advanced_view;

private:
	void setup_menu_bar();
};


#endif //SERIALBOX_MAINWINDOWVIEW_HPP
