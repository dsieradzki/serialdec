
#ifndef SERIALDEC_BASICVIEW_HPP
#define SERIALDEC_BASICVIEW_HPP

#include <wx/notebook.h>
#include <wx/wx.h>
#include "DataView.hpp"

class BasicView : public wxNotebookPage {
public:
	explicit BasicView(wxWindow *parent,
	                   wxWindowID id,
	                   const wxPoint &pos = wxDefaultPosition,
	                   const wxSize &size = wxDefaultSize,
	                   long style = 0,
	                   const wxString &name = wxPanelNameStr);

private:
	friend class BasicViewPresenter;

	sd::DataView *m_data_view;
	wxTextCtrl *m_data_entry;
	wxCheckBox *m_enter_send_btn;
	wxButton *m_clear_button;
	wxButton *m_send_button;
};


#endif //SERIALDEC_BASICVIEW_HPP
