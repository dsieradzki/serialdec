
#ifndef SERIALDEC_REPRESENTATIONVIEW_HPP
#define SERIALDEC_REPRESENTATIONVIEW_HPP


#include <wx/wx.h>
#include "../../converter/Representation.hpp"
#include <serial/SerialPort.hpp>

namespace sd {
	class DataView : public wxPanel {
	public:
		explicit DataView(wxWindow *parent,
		                   wxWindowID winid = wxID_ANY,
		                   const wxPoint &pos = wxDefaultPosition,
		                   const wxSize &size = wxDefaultSize,
		                   long style = wxTAB_TRAVERSAL | wxNO_BORDER,
		                   const wxString &name = wxPanelNameStr);


		void clear();
		bool is_empty() const;
		std::regex validation_regex_for_data_entry();
		std::string tooltip_for_data_entry() const;
		wxString data_entry_hint() const;

		dstools::byte_array convert_to_send(const std::string &input);
		void append_outgoing_to_show(std::string data);
		void append_ingoing_to_show(const dstools::byte_array &data);
	private:
		wxTextCtrl *m_text_area;
		std::unique_ptr<Representation> m_representation;
	};

}

#endif //SERIALDEC_REPRESENTATIONVIEW_HPP
