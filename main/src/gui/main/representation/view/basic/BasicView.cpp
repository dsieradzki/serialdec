
#include "BasicView.hpp"
#include "../../../../SerialDeCApplication.hpp"


BasicView::BasicView(wxWindow *parent,
                     wxWindowID id,
                     const wxPoint &pos,
                     const wxSize &size,
                     long style,
                     const wxString &name) : wxWindow(parent, id, pos, size, style, name) {

	auto *container_sizer = new wxBoxSizer(wxVERTICAL);
	SetSizer(container_sizer);


	m_data_view = new sd::DataView(this);

	auto *bottomContainer = new wxBoxSizer(wxHORIZONTAL);
	m_data_entry = new wxTextCtrl(this, wxID_ANY);
	m_data_entry->SetFont(SerialDeCApplication::DATA_FONT);

	m_send_button = new wxButton(this, wxID_ANY, wxT("Send"));
	m_clear_button = new wxButton(this, wxID_ANY, wxT("Clear"));

	m_enter_send_btn = new wxCheckBox(this, wxID_ANY, "Enter sends");


	container_sizer->Add(m_data_view, 1, wxEXPAND | wxLEFT | wxRIGHT, 10);

	bottomContainer->Add(m_data_entry, 1, wxEXPAND | wxALL, 10);

	bottomContainer->Add(m_enter_send_btn, wxSizerFlags()
			.Proportion(0)
			.Align(wxALIGN_CENTER_VERTICAL)
			.Border(wxRIGHT | wxTOP | wxBOTTOM, 10));

	bottomContainer->Add(m_send_button, 0, wxRIGHT | wxTOP | wxBOTTOM, 10);
	bottomContainer->Add(m_clear_button, 0, wxRIGHT | wxTOP | wxBOTTOM, 10);

	container_sizer->Add(bottomContainer, 0, wxEXPAND);

}

