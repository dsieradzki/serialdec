#include <utility>


#ifndef SERIALDEC_BASICVIEWPRESENTER_HPP
#define SERIALDEC_BASICVIEWPRESENTER_HPP


#include "BasicView.hpp"
#include <future>
#include <memory>
#include <serial/SerialPort.hpp>

class BasicViewPresenter {
public:
	explicit BasicViewPresenter(BasicView &view);

	void on_change_data_to_send(wxKeyEvent &event);
	void on_click_send_button(wxCommandEvent &event);
	void update_component_states();

	void update_connection_status(bool connectionState);
	void add_data(const dstools::byte_array &p_data);
	void set_on_send_data_listener(std::function<void(const dstools::byte_array &)> p_on_send_data);
	void set_on_enter_send_mode_listener(std::function<void(bool)> p_on_enter_send_mode);
	void set_enter_sends(bool state);
private:
	bool enter_sends;
	bool m_connection_state;
	BasicView& m_view;
	std::future<void> m_send_data_task;

	std::function<void(const dstools::byte_array&)> m_on_send_data;
	std::function<void(bool)> m_on_enter_send_mode_listener;

	void bind_actions();
	void configure();
};


#endif //SERIALDEC_BASICVIEWPRESENTER_HPP
