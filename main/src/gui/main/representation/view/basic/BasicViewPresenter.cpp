
#include "BasicViewPresenter.hpp"
#include "../../../../SerialPortProvider.hpp"


wxDEFINE_EVENT(EVT_UPDATE_TEXT_AREA, wxCommandEvent);

BasicViewPresenter::BasicViewPresenter(BasicView &view) : m_view(view) {
	bind_actions();
	configure();
}

void BasicViewPresenter::configure() {
	m_view.m_data_entry->SetToolTip(wxString(m_view.m_data_view->tooltip_for_data_entry()));
	m_view.m_data_entry->SetHint(m_view.m_data_view->data_entry_hint());
}

void BasicViewPresenter::bind_actions() {
	m_view.m_data_entry->Bind(wxEVT_KEY_UP, &BasicViewPresenter::on_change_data_to_send, this);
	m_view.m_data_entry->Bind(wxEVT_KEY_DOWN, &BasicViewPresenter::on_change_data_to_send, this);
	m_view.m_send_button->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &BasicViewPresenter::on_click_send_button, this);
	m_view.m_clear_button->Bind(wxEVT_COMMAND_BUTTON_CLICKED, [=](wxCommandEvent& WXUNUSED(event)) {
		m_view.m_data_view->clear();
		update_component_states();
	});
	m_view.Bind(EVT_UPDATE_TEXT_AREA, [=](wxCommandEvent &event) {
		if (!event.GetClientData()) {
			return;
		}
		std::unique_ptr<dstools::byte_array> data(static_cast<dstools::byte_array*>(event.GetClientData()));
		m_view.m_data_view->append_ingoing_to_show(*data);
		update_component_states();
	});

	m_view.m_enter_send_btn->Bind(wxEVT_CHECKBOX, [=](wxCommandEvent & WXUNUSED(event)) {
		this->enter_sends = m_view.m_enter_send_btn->IsChecked();
		if (m_on_enter_send_mode_listener) {
			m_on_enter_send_mode_listener(this->enter_sends);
		}
	});

	m_view.m_data_entry->Bind(wxEVT_KEY_DOWN, [=](wxKeyEvent &event) {

		if (this->enter_sends && event.GetKeyCode() == wxKeyCode::WXK_RETURN) {
			wxCommandEvent event_to_click;
			on_click_send_button(event_to_click);
			m_view.m_data_entry->SetFocus();
			event.Skip(false);
		} else {
			event.Skip();
		}
	});
}


void BasicViewPresenter::on_change_data_to_send(wxKeyEvent &event) {
	event.Skip();
	if (std::regex_match(m_view.m_data_entry->GetValue().ToStdString(), m_view.m_data_view->validation_regex_for_data_entry())) {
		m_view.m_send_button->Enable();
	} else {
		m_view.m_send_button->Disable();
	}
}

void BasicViewPresenter::on_click_send_button(wxCommandEvent& WXUNUSED(event)) {
	auto status = std::future_status::ready;
	const std::string data = m_view.m_data_entry->GetValue().ToStdString();

	if (data.empty()) {
		return;
	}

	m_view.m_send_button->Disable();

	if (m_send_data_task.valid()) {
		status = m_send_data_task.wait_for(std::chrono::milliseconds(0));
	}

	if (status == std::future_status::ready) {
		m_send_data_task = std::async(std::launch::async, [=]() {
			const dstools::byte_array data_to_send = m_view.m_data_view->convert_to_send(data);
			SERIAL_PORT->write(data_to_send);
			if(m_on_send_data) {
				m_on_send_data(data_to_send);
			}

			m_view.GetEventHandler()->CallAfter([=]() {
				m_view.m_data_view->append_outgoing_to_show(data);
				m_view.m_data_entry->Clear();
				update_component_states();
			});
		});
	}
}

void BasicViewPresenter::update_component_states() {
	m_view.m_data_entry->Enable(m_connection_state);
	m_view.m_send_button->Enable(m_connection_state && !m_view.m_data_entry->IsEmpty());
	m_view.m_clear_button->Enable(!m_view.m_data_view->is_empty());
}

void BasicViewPresenter::add_data(const dstools::byte_array &p_data) {
	auto *data = new dstools::byte_array();
	data->reserve(p_data.size());
	data->assign(p_data.begin(), p_data.end());

	auto *evt = new wxCommandEvent();
	evt->SetClientData(data);
	evt->SetEventType(EVT_UPDATE_TEXT_AREA);
	m_view.QueueEvent(evt);
}

void BasicViewPresenter::update_connection_status(bool connectionState) {
	m_connection_state = connectionState;
	update_component_states();
}

void BasicViewPresenter::set_on_send_data_listener(std::function<void(const dstools::byte_array &)> p_on_send_data) {
	this->m_on_send_data = std::move(p_on_send_data);
}

void BasicViewPresenter::set_on_enter_send_mode_listener(std::function<void(bool)> p_on_enter_send_mode) {
	this->m_on_enter_send_mode_listener = std::move(p_on_enter_send_mode);
}

void BasicViewPresenter::set_enter_sends(bool state) {
	this->enter_sends = state;
	this->m_view.m_enter_send_btn->SetValue(state);
}
