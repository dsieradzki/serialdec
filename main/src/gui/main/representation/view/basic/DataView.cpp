
#include <sstream>
#include "DataView.hpp"
#include "../../converter/ASCIIRepresentation.hpp"
#include "../../converter/HexRepresentation.hpp"
#include "../../converter/BinaryRepresentation.hpp"
#include "../../../../SerialDeCApplication.hpp"

sd::DataView::DataView(wxWindow *parent,
                       wxWindowID winid,
                       const wxPoint &pos,
                       const wxSize &size,
                       long style,
                       const wxString &name) : wxPanel(parent,
                                                       winid,
                                                       pos,
                                                       size,
                                                       style,
                                                       name) {

	auto *container_sizer = new wxBoxSizer(wxVERTICAL);
	SetSizer(container_sizer);


	m_text_area = new wxTextCtrl(this, wxID_ANY, wxT(""), wxDefaultPosition, wxSize(250, 150), wxTE_MULTILINE);
	m_text_area->SetEditable(false);
	m_text_area->SetFont(SerialDeCApplication::DATA_FONT);
	container_sizer->Add(m_text_area, 1, wxEXPAND);
	m_representation = std::unique_ptr<Representation>(new ASCIIRepresentation());
}

void sd::DataView::clear() {
	m_text_area->Clear();
}

bool sd::DataView::is_empty() const {
	return m_text_area->IsEmpty();
}


std::regex sd::DataView::validation_regex_for_data_entry() {
	if (!m_representation) {
		return std::regex();
	}
	return m_representation->validation_regex_for_data_entry();
}

std::string sd::DataView::tooltip_for_data_entry() const {
	return m_representation->tooltip_for_data_entry();
}

wxString sd::DataView::data_entry_hint() const {
	return m_representation->data_entry_hint();
}


dstools::byte_array sd::DataView::convert_to_send(const std::string &input) {
	return m_representation->convert_to_send(input);
}

void sd::DataView::append_outgoing_to_show(std::string data) {
	wxString data1 = m_representation->parse_outgoing_to_show(data);
	m_text_area->AppendText(data1);
}

void sd::DataView::append_ingoing_to_show(const dstools::byte_array &data) {
	m_text_area->AppendText(m_representation->parse_ingoing_to_show(data));
}
