#ifndef SERIALDEC_ADVANCEDVIEWPRESENTER_HPP
#define SERIALDEC_ADVANCEDVIEWPRESENTER_HPP


#include "AdvancedView.hpp"
#include <serial/SerialPort.hpp>

class AdvancedViewPresenter {
public:
	explicit AdvancedViewPresenter(AdvancedView &p_view);

	void update_connection_status(bool connectionState);;
	void set_on_send_data_listener(std::function<void(const dstools::byte_array &)> p_on_send_data);
	void set_on_enter_send_mode_listener(std::function<void(bool)> p_on_enter_send_mode);
	void add_data(const dstools::byte_array &p_data);
	void set_enter_sends(bool state);
private:
	bool enter_sends;
	AdvancedView &m_view;
	std::unique_ptr<Representation> m_text_representation;
	std::unique_ptr<Representation> m_hex_representation;
	std::unique_ptr<Representation> m_binary_representation;

	bool m_connection_state;
	SelectedRepresentation m_selected_representation;

	std::function<void(const dstools::byte_array &)> m_on_send_data_listener;
	std::function<void(bool)> m_on_enter_send_mode_listener;

	std::future<void> m_send_data_task;

	void bind_actions();
	void configure();
	Representation &get_active_representation();
	void update_component_states();
	void on_change_data_to_send(wxKeyEvent &event);

	void on_click_send_button(wxCommandEvent &event);
	std::regex validation_regex_for_data_entry();
	void append_outgoing_to_show(const std::string &data);
};


#endif //SERIALDEC_ADVANCEDVIEWPRESENTER_HPP
