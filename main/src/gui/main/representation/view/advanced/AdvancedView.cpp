
#include "AdvancedView.hpp"
#include "../../../../SerialDeCApplication.hpp"

AdvancedView::AdvancedView(wxWindow *parent,
                           wxWindowID id,
                           const wxPoint &pos,
                           const wxSize &size,
                           long style,
                           const wxString &name) : wxWindow(parent, id, pos, size, style, name) {

	auto rootSizer = new wxBoxSizer(wxVERTICAL);
	SetSizer(rootSizer);

	auto modesSizer = new wxBoxSizer(wxHORIZONTAL);
	m_data_sizer = new wxBoxSizer(wxHORIZONTAL);
	auto bottomSizer = new wxBoxSizer(wxHORIZONTAL);

	setup_data_gui(m_data_sizer);
	setup_bottom_gui(bottomSizer);

	rootSizer->Add(modesSizer, 0, wxEXPAND);
	rootSizer->Add(m_data_sizer, 1, wxEXPAND);
	rootSizer->Add(bottomSizer, 0, wxEXPAND);
}


void AdvancedView::setup_data_gui(wxBoxSizer *pSizer) {
	m_text_mode_data = new wxTextCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);
	m_hex_mode_data = new wxTextCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);
	m_binary_mode_data = new wxTextCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);

	m_text_mode_data->SetFont(SerialDeCApplication::DATA_FONT);
	m_hex_mode_data->SetFont(SerialDeCApplication::DATA_FONT);
	m_binary_mode_data->SetFont(SerialDeCApplication::DATA_FONT);

	m_text_mode_data->SetEditable(false);
	m_hex_mode_data->SetEditable(false);
	m_binary_mode_data->SetEditable(false);


	wxStaticText *text_mode_label = new wxStaticText(this, wxID_ANY, "Ascii:");
	wxStaticText *hex_mode_label = new wxStaticText(this, wxID_ANY, "Hex:");
	wxStaticText *bin_mode_label = new wxStaticText(this, wxID_ANY, "Binary:");


	auto *text_mode_sizer = new wxBoxSizer(wxVERTICAL);
	auto *hex_mode_sizer = new wxBoxSizer(wxVERTICAL);
	auto *bin_mode_sizer = new wxBoxSizer(wxVERTICAL);

	text_mode_sizer->Add(text_mode_label, wxSizerFlags().Border(wxBOTTOM, 5));
	text_mode_sizer->Add(m_text_mode_data, wxSizerFlags().Proportion(1).Expand());

	hex_mode_sizer->Add(hex_mode_label, wxSizerFlags().Border(wxBOTTOM, 5));
	hex_mode_sizer->Add(m_hex_mode_data, wxSizerFlags().Proportion(1).Expand());

	bin_mode_sizer->Add(bin_mode_label, wxSizerFlags().Border(wxBOTTOM, 5));
	bin_mode_sizer->Add(m_binary_mode_data, wxSizerFlags().Proportion(1).Expand());


	pSizer->Add(text_mode_sizer, wxSizerFlags().Proportion(1).Expand().Border(wxLEFT | wxBOTTOM, 10));
	pSizer->Add(hex_mode_sizer, wxSizerFlags().Proportion(1).Expand().Border(wxBOTTOM, 10));
	pSizer->Add(bin_mode_sizer, wxSizerFlags().Proportion(1).Expand().Border(wxRIGHT | wxBOTTOM, 10));

}

void AdvancedView::setup_bottom_gui(wxBoxSizer *pSizer) {

	wxSize modeBtnSize(50, -1);
	m_text_mode_btn = new wxToggleButton(this, wxID_ANY, "Ascii", wxDefaultPosition, modeBtnSize);
	m_hex_mode_btn = new wxToggleButton(this, wxID_ANY, "Hex", wxDefaultPosition, modeBtnSize);
	m_bin_mode_btn = new wxToggleButton(this, wxID_ANY, "Bin", wxDefaultPosition, modeBtnSize);

	m_enter_send_btn = new wxCheckBox(this, wxID_ANY, "Enter sends");


	m_data_entry = new wxTextCtrl(this, wxID_ANY);
	m_data_entry->SetFont(SerialDeCApplication::DATA_FONT);

	m_send_button = new wxButton(this, wxID_ANY, "Send");
	m_clear_button = new wxButton(this, wxID_ANY, "Clear");

	pSizer->Add(m_text_mode_btn, 0, wxLEFT, 10);
	pSizer->Add(m_hex_mode_btn, 0);
	pSizer->Add(m_bin_mode_btn, 0);


	pSizer->Add(m_data_entry,
	            wxSizerFlags()
			            .Proportion(1)
			            .Expand()
			            .Border(wxLEFT | wxRIGHT | wxBOTTOM, 10));

	pSizer->Add(m_enter_send_btn, wxSizerFlags()
			.Proportion(0)
			.Align(wxALIGN_CENTER_VERTICAL)
			.Border(wxRIGHT | wxBOTTOM, 10));

	pSizer->Add(m_send_button, wxSizerFlags()
			.Proportion(0)
			.Border(wxRIGHT | wxBOTTOM, 10));

	pSizer->Add(m_clear_button, wxSizerFlags()
			.Proportion(0)
			.Border(wxRIGHT | wxBOTTOM, 10));
}


