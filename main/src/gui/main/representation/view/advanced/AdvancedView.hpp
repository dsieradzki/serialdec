
#ifndef SERIALDEC_ADVANCEDVIEW_HPP
#define SERIALDEC_ADVANCEDVIEW_HPP

#include <wx/notebook.h>
#include <wx/wx.h>
#include <wx/tglbtn.h>
#include <future>
#include "../../converter/Representation.hpp"

enum class SelectedRepresentation { TEXT, HEX, BINARY };

class AdvancedView : public wxNotebookPage {
public:
	explicit AdvancedView(wxWindow *parent,
	                      wxWindowID id,
	                      const wxPoint &pos = wxDefaultPosition,
	                      const wxSize &size = wxDefaultSize,
	                      long style = 0,
	                      const wxString &name = wxPanelNameStr);


private:
	friend class AdvancedViewPresenter;

	wxTextCtrl *m_text_mode_data;
	wxTextCtrl *m_hex_mode_data;
	wxTextCtrl *m_binary_mode_data;

	wxTextCtrl *m_data_entry;
	wxButton *m_send_button;
	wxButton *m_clear_button;

	wxToggleButton *m_text_mode_btn;
	wxToggleButton *m_hex_mode_btn;
	wxToggleButton *m_bin_mode_btn;

	wxCheckBox *m_enter_send_btn;

	wxBoxSizer *m_data_sizer;

	void setup_data_gui(wxBoxSizer *pSizer);
	void setup_bottom_gui(wxBoxSizer *pSizer);

};


#endif //SERIALDEC_ADVANCEDVIEW_HPP
