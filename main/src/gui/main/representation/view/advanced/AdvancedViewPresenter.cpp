
#include <logger/LoggerProvider.hpp>
#include "AdvancedViewPresenter.hpp"
#include "../../converter/ASCIIRepresentation.hpp"
#include "../../converter/HexRepresentation.hpp"
#include "../../converter/BinaryRepresentation.hpp"
#include "../../../../SerialPortProvider.hpp"

wxDEFINE_EVENT(EVT_ADVANCED_VIEW_UPDATE_TEXT_AREA, wxCommandEvent);

AdvancedViewPresenter::AdvancedViewPresenter(AdvancedView &p_view) : m_view(p_view) {
	bind_actions();
	configure();
}

void AdvancedViewPresenter::bind_actions() {
	m_view.m_send_button->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &AdvancedViewPresenter::on_click_send_button, this);
	m_view.m_data_entry->Bind(wxEVT_KEY_UP, &AdvancedViewPresenter::on_change_data_to_send, this);
	m_view.m_data_entry->Bind(wxEVT_KEY_DOWN, &AdvancedViewPresenter::on_change_data_to_send, this);
	m_view.m_clear_button->Bind(wxEVT_COMMAND_BUTTON_CLICKED, [=](wxCommandEvent & WXUNUSED(event)) {
		m_view.m_text_mode_data->Clear();
		m_view.m_hex_mode_data->Clear();
		m_view.m_binary_mode_data->Clear();
		update_component_states();
	});

	m_view.m_text_mode_btn->Bind(wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, [this](wxCommandEvent & WXUNUSED(event)) {
		m_view.m_text_mode_btn->Disable();
		m_view.m_hex_mode_btn->Enable();
		m_view.m_bin_mode_btn->Enable();

		m_view.m_hex_mode_btn->SetValue(false);
		m_view.m_bin_mode_btn->SetValue(false);

		m_view.m_data_entry->SetToolTip(m_text_representation->tooltip_for_data_entry());
		m_view.m_data_entry->SetHint(m_text_representation->data_entry_hint());
		m_selected_representation = SelectedRepresentation::TEXT;
	});

	m_view.m_hex_mode_btn->Bind(wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, [this](wxCommandEvent & WXUNUSED(event)) {
		m_view.m_text_mode_btn->Enable();
		m_view.m_hex_mode_btn->Disable();
		m_view.m_bin_mode_btn->Enable();

		m_view.m_text_mode_btn->SetValue(false);
		m_view.m_bin_mode_btn->SetValue(false);

		m_view.m_data_entry->SetToolTip(m_hex_representation->tooltip_for_data_entry());
		m_view.m_data_entry->SetHint(m_hex_representation->data_entry_hint());
		m_selected_representation = SelectedRepresentation::HEX;
	});

	m_view.m_bin_mode_btn->Bind(wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, [this](wxCommandEvent & WXUNUSED(event)) {
		m_view.m_text_mode_btn->Enable();
		m_view.m_hex_mode_btn->Enable();
		m_view.m_bin_mode_btn->Disable();

		m_view.m_text_mode_btn->SetValue(false);
		m_view.m_hex_mode_btn->SetValue(false);

		m_view.m_data_entry->SetToolTip(m_binary_representation->tooltip_for_data_entry());
		m_view.m_data_entry->SetHint(m_binary_representation->data_entry_hint());
		m_selected_representation = SelectedRepresentation::BINARY;
	});

	m_view.Bind(EVT_ADVANCED_VIEW_UPDATE_TEXT_AREA, [=](wxCommandEvent &event) {
		if (!event.GetClientData()) {
			return;
		}
		std::unique_ptr<dstools::byte_array> data(static_cast<dstools::byte_array *>(event.GetClientData()));
		m_view.m_text_mode_data->AppendText(m_text_representation->parse_ingoing_to_show(*data));
		m_view.m_hex_mode_data->AppendText(m_hex_representation->parse_ingoing_to_show(*data));
		m_view.m_binary_mode_data->AppendText(m_binary_representation->parse_ingoing_to_show(*data));
		update_component_states();
	});

	m_view.m_enter_send_btn->Bind(wxEVT_CHECKBOX, [=](wxCommandEvent & WXUNUSED(event)) {
		this->enter_sends = m_view.m_enter_send_btn->IsChecked();

		if (m_on_enter_send_mode_listener) {
			m_on_enter_send_mode_listener(this->enter_sends);
		}
	});

	m_view.m_data_entry->Bind(wxEVT_KEY_DOWN, [=](wxKeyEvent &event) {

		if (this->enter_sends && event.GetKeyCode() == wxKeyCode::WXK_RETURN) {
			wxCommandEvent event_to_click;
			on_click_send_button(event_to_click);
			m_view.m_data_entry->SetFocus();
			event.Skip(false);
		} else {
			event.Skip();
		}
	});
}

void AdvancedViewPresenter::configure() {
	m_text_representation = std::make_unique<ASCIIRepresentation>();
	m_hex_representation = std::make_unique<HexRepresentation>();
	m_binary_representation = std::make_unique<BinaryRepresentation>();

	m_view.m_text_mode_btn->SetValue(true);
	m_view.m_text_mode_btn->Disable();
	m_view.m_data_entry->SetHint(m_text_representation->data_entry_hint());
	m_selected_representation = SelectedRepresentation::TEXT;
}

void AdvancedViewPresenter::on_change_data_to_send(wxKeyEvent &event) {
	event.Skip();
	if (std::regex_match(m_view.m_data_entry->GetValue().ToStdString(), validation_regex_for_data_entry())) {
		m_view.m_send_button->Enable();
	} else {
		m_view.m_send_button->Disable();
	}
}

void AdvancedViewPresenter::on_click_send_button(wxCommandEvent & WXUNUSED(event)) {
	auto status = std::future_status::ready;
	const std::string data = m_view.m_data_entry->GetValue().ToStdString();

	if (data.empty()) {
		return;
	}

	m_view.m_send_button->Disable();

	if (m_send_data_task.valid()) {
		status = m_send_data_task.wait_for(std::chrono::milliseconds(0));
	}

	if (status == std::future_status::ready) {
		m_send_data_task = std::async(std::launch::async, [=]() {
			const dstools::byte_array data_to_send = get_active_representation().convert_to_send(data);
			SERIAL_PORT->write(data_to_send);
			if (m_on_send_data_listener) {
				m_on_send_data_listener(data_to_send);
			}

			m_view.GetEventHandler()->CallAfter([=]() {
				append_outgoing_to_show(data);
				m_view.m_data_entry->Clear();
				update_component_states();
			});
		});
	}
}

Representation &AdvancedViewPresenter::get_active_representation() {
	switch (m_selected_representation) {
		case SelectedRepresentation::TEXT:
			return *m_text_representation.get();
		case SelectedRepresentation::HEX:
			return *m_hex_representation.get();
		case SelectedRepresentation::BINARY:
			return *m_binary_representation.get();
		default:
			LOG_ERROR("Cannot determine representation");
			throw std::runtime_error("Cannot determine representation");
	}
}


void AdvancedViewPresenter::append_outgoing_to_show(const std::string &p_data) {
	const dstools::byte_array data_as_bytes = get_active_representation().convert_to_send(p_data);

	m_view.m_text_mode_data->AppendText(m_text_representation->parse_ingoing_to_show(data_as_bytes));
	m_view.m_hex_mode_data->AppendText(m_hex_representation->parse_ingoing_to_show(data_as_bytes));
	m_view.m_binary_mode_data->AppendText(m_binary_representation->parse_ingoing_to_show(data_as_bytes));

}

void AdvancedViewPresenter::update_component_states() {
	m_view.m_data_entry->Enable(m_connection_state);
	m_view.m_send_button->Enable(m_connection_state && !m_view.m_data_entry->IsEmpty());
	m_view.m_clear_button->Enable(!m_view.m_text_mode_data->IsEmpty() || !m_view.m_hex_mode_data->IsEmpty() || !m_view.m_binary_mode_data->IsEmpty());
}

std::regex AdvancedViewPresenter::validation_regex_for_data_entry() {
	switch (m_selected_representation) {
		case SelectedRepresentation::TEXT:
			return m_text_representation->validation_regex_for_data_entry();
		case SelectedRepresentation::HEX:
			return m_hex_representation->validation_regex_for_data_entry();
		case SelectedRepresentation::BINARY:
			return m_binary_representation->validation_regex_for_data_entry();
		default:
			LOG_ERROR("Cannot validate value for unknown representation");
			throw std::runtime_error("Cannot validate value for unknown representation");
	}
}

void AdvancedViewPresenter::add_data(const dstools::byte_array &p_data) {
	auto *data = new dstools::byte_array();
	data->reserve(p_data.size());
	data->assign(p_data.begin(), p_data.end());

	auto *evt = new wxCommandEvent();
	evt->SetClientData(data);
	evt->SetEventType(EVT_ADVANCED_VIEW_UPDATE_TEXT_AREA);
	m_view.QueueEvent(evt);
}

void AdvancedViewPresenter::update_connection_status(bool connectionState) {
	m_connection_state = connectionState;
	update_component_states();
}

void AdvancedViewPresenter::set_on_send_data_listener(std::function<void(const dstools::byte_array &)> p_on_send_data) {
	this->m_on_send_data_listener = std::move(p_on_send_data);
}

void AdvancedViewPresenter::set_on_enter_send_mode_listener(std::function<void(bool)> p_on_enter_send_mode) {
	this->m_on_enter_send_mode_listener = std::move(p_on_enter_send_mode);
}

void AdvancedViewPresenter::set_enter_sends(bool state) {
	this->enter_sends = state;
	this->m_view.m_enter_send_btn->SetValue(state);
}

