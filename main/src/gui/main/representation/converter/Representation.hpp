
#ifndef SERIALBOX_REPRESENTATION_HPP
#define SERIALBOX_REPRESENTATION_HPP


#include <string>
#include <vector>
#include <cstdint>
#include <regex>
#include <serial/SerialPort.hpp>

class Representation {
public:
	virtual ~Representation() = default;
	virtual std::string parse_ingoing_to_show(const dstools::byte_array &data) = 0;
	virtual dstools::byte_array convert_to_send(const std::string &data) = 0;
	virtual std::string parse_outgoing_to_show(const std::string &data) = 0;
	virtual std::string tooltip_for_data_entry() = 0;
	virtual std::regex validation_regex_for_data_entry() = 0;
	virtual std::string data_entry_hint() = 0;
};


#endif //SERIALBOX_REPRESENTATION_HPP
