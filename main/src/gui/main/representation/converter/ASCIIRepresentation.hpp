
#ifndef SERIALBOX_INOUTREPRESENTATION_HPP
#define SERIALBOX_INOUTREPRESENTATION_HPP


#include <iostream>
#include <map>
#include "Representation.hpp"

class ASCIIRepresentation : public Representation {
public:
	std::string parse_ingoing_to_show(const dstools::byte_array &data) override;
	dstools::byte_array convert_to_send(const std::string &data) override;
	std::string parse_outgoing_to_show(const std::string &data) override;
	std::string tooltip_for_data_entry() override;
	std::regex validation_regex_for_data_entry() override;
	std::string data_entry_hint() override;
private:
	std::string convert_special_character_literals(const std::string &data);
	std::map<std::string, std::string> SPECIAL_LITERALS = {
			{"\\n", "\n"},
			{"\\r", "\r"},
			{"\\t", "\t"}
	};
};


#endif //SERIALBOX_INOUTREPRESENTATION_HPP
