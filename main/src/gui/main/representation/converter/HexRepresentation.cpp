
#include "HexRepresentation.hpp"
#include "../../../../core/StringUtil.hpp"
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <iostream>
#include <cctype>

std::string HexRepresentation::parse_ingoing_to_show(const dstools::byte_array &p_data) {
	std::ostringstream ret;
	for (dstools::byte e : p_data) {
		ret << std::hex << std::setfill('0') << std::setw(2) << std::uppercase << static_cast<int>(e) << " ";
	}
	return ret.str();
}

dstools::byte_array HexRepresentation::convert_to_send(const std::string &data) {
	std::string data_to_send(data);
	data_to_send.erase(std::remove(data_to_send.begin(), data_to_send.end(), ' '), data_to_send.end());

	dstools::byte_array result;
	result.reserve(data_to_send.size());


	for (unsigned int i = 0; i < data_to_send.size(); i += 2) {
		auto bigPart = sd::character::to_string(data_to_send[i]);
		auto smallPart = sd::character::to_string(data_to_send[i + 1]);
		result.emplace_back(static_cast<dstools::byte>(std::stoul(bigPart + smallPart, nullptr, 16)));
	}
	return result;
}

std::string HexRepresentation::parse_outgoing_to_show(const std::string &data) {
	std::string dataToShow(data);
	dataToShow.erase(std::remove(dataToShow.begin(), dataToShow.end(), ' '), dataToShow.end());

	std::ostringstream ret;

	for (std::size_t i = 0; i < dataToShow.size(); i += 2) {
		ret << dataToShow[i] << dataToShow[i + 1] << " ";
	}
	auto result = ret.str();
	std::transform(result.begin(), result.end(), result.begin(), ::toupper);
	return result;
}

std::string HexRepresentation::tooltip_for_data_entry() {
	return "Hexadecimal format of data. ex. 05 A5 FE";
}

std::regex HexRepresentation::validation_regex_for_data_entry() {
	return std::regex("^[0-9|a-f|A-F| ]+$");
}

std::string HexRepresentation::data_entry_hint() {
	return "FC AB...";
}
