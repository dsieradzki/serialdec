
#include "ASCIIRepresentation.hpp"
#include "../../../../core/StringUtil.hpp"

std::string ASCIIRepresentation::parse_ingoing_to_show(const dstools::byte_array &data) {
	std::stringstream ss;
	for (dstools::byte i : data) {
		if ((int) i > 127) {
			ss << ".";
		} else {
			ss << std::string(1, (unsigned char) i);
		}
	}

	return ss.str();
}

dstools::byte_array ASCIIRepresentation::convert_to_send(const std::string &data) {
	auto to_send = convert_special_character_literals(data);
	return sd::byte_array::to_unsigned_char::convert(to_send);
}

std::string ASCIIRepresentation::parse_outgoing_to_show(const std::string &data) {
	return convert_special_character_literals(data);
}

std::string ASCIIRepresentation::tooltip_for_data_entry() {
	return R"(Text format of data. You can fill with any text. Special character like \n \r \t are supported)";
}

std::regex ASCIIRepresentation::validation_regex_for_data_entry() {
	return std::regex(R"(^[\x00-\x7F]+$)");
}

std::string ASCIIRepresentation::convert_special_character_literals(const std::string &data) {
	std::string result(data);
	for (std::pair<std::string, std::string> e: SPECIAL_LITERALS) {
		unsigned long index;

		while ((index = result.find(e.first)) != std::string::npos) {
			result.replace(index, e.first.size(), e.second);
		}

	}
	return result;
}

std::string ASCIIRepresentation::data_entry_hint() {
	return "to send\\n...";
}
