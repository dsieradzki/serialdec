
#include <sstream>
#include <bitset>
#include <algorithm>
#include "BinaryRepresentation.hpp"


std::string BinaryRepresentation::parse_ingoing_to_show(const dstools::byte_array &p_data) {
	std::ostringstream ret;
	for (dstools::byte e : p_data) {
		ret << std::bitset<8>(e) << " ";
	}
	return ret.str();
}

dstools::byte_array BinaryRepresentation::convert_to_send(const std::string &data) {
	std::vector<std::string> splitted_data = format_binary_string(data);

	dstools::byte_array result;
	result.reserve(splitted_data.size());

	for (const std::string& element : splitted_data) {
		unsigned long long int e_res = std::strtoull(element.c_str(), nullptr, 2);
		result.emplace_back(static_cast<dstools::byte>(e_res));
	}

	return result;
}

std::string BinaryRepresentation::parse_outgoing_to_show(const std::string &data) {
	std::vector<std::string> splitted_data = format_binary_string(data);
	std::ostringstream result;
	for (const std::string &e : splitted_data) {
		result << e << " ";
	}
	return result.str();
}

std::vector<std::string> BinaryRepresentation::format_binary_string(const std::string &data) const {
	std::string data_to_show(data);
	data_to_show.erase(remove(data_to_show.begin(), data_to_show.end(), ' '), data_to_show.end());
	std::vector<std::string> splitted_data;

	for (unsigned long i = 0; i < data_to_show.size(); i += 8) {
		auto item = data_to_show.substr(i, std::min(8ul, data_to_show.size() - i));
		if (item.size() < 8) {
			item = std::string(8 - item.size(), '0') + item;
		}
		splitted_data.emplace_back(item);
	}
	return splitted_data;
}

std::string BinaryRepresentation::tooltip_for_data_entry() {
	return "Binary format of data. ex. 01010101 0101 0101";
}

std::regex BinaryRepresentation::validation_regex_for_data_entry() {
	return std::regex("^[01 ]+$");
}

std::string BinaryRepresentation::data_entry_hint() {
	return "101...";
}
