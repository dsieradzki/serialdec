
#ifndef SERIALBOX_BINARYREPRESENTATION_HPP
#define SERIALBOX_BINARYREPRESENTATION_HPP


#include "Representation.hpp"

class BinaryRepresentation: public Representation {
public:
	std::string parse_ingoing_to_show(const dstools::byte_array &p_data) override;
	dstools::byte_array convert_to_send(const std::string &data) override;
	std::string parse_outgoing_to_show(const std::string &data) override;
	std::string tooltip_for_data_entry() override;
	std::regex validation_regex_for_data_entry() override;
	std::string data_entry_hint() override;
private:
	std::vector<std::string> format_binary_string(const std::string &data) const;
};


#endif //SERIALBOX_BINARYREPRESENTATION_HPP
