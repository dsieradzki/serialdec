
#include "MainWindowView.hpp"
#include "connection/ConnectionView.hpp"
#include "representation/view/basic/BasicView.hpp"
#include "representation/view/advanced/AdvancedView.hpp"
#include "representation/view/basic/BasicViewPresenter.hpp"

#include <wx/notebook.h>

MainWindowView::MainWindowView(const wxString &title) : wxFrame(nullptr, wxID_ANY, title, wxDefaultPosition,
                                                                wxSize(820, 600)) {

	setup_menu_bar();
	this->SetMinSize(wxSize(450, 250));

	auto *container = new wxPanel(this, wxID_ANY);
	auto *container_sizer = new wxBoxSizer(wxVERTICAL);
	container->SetSizer(container_sizer);



	//TOP END
	m_connection_view = new sd::ConnectionView(container);
	container_sizer->Add(m_connection_view, 0, wxEXPAND | wxLEFT| wxTOP | wxRIGHT, 10);


	//CENTER
	auto mainComponent = new wxNotebook(container, wxID_ANY, wxDefaultPosition, wxSize(500, 500), wxNB_MULTILINE);

	this->m_basic_view = new BasicView(mainComponent, -1);
	this->m_advanced_view = new AdvancedView(mainComponent, -1);

	mainComponent->AddPage(m_basic_view, L"Basic");
	mainComponent->AddPage(m_advanced_view, L"Advanced");

	container_sizer->Add(mainComponent, 1, wxEXPAND | wxLEFT | wxRIGHT, 10);

	Centre();
}

void MainWindowView::setup_menu_bar() {
	m_menubar = new wxMenuBar();

	auto *file_menu = new wxMenu();
	file_menu->Append(wxID_EXIT, wxT("&Quit"));

	auto *help_menu = new wxMenu();
	help_menu->Append(wxID_ABOUT, wxT("About"));

	m_menubar->Append(file_menu, wxT("&File"));
	m_menubar->Append(help_menu, wxT("&Help"));
	SetMenuBar(m_menubar);
}
