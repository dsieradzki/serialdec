#include "MainWindowPresenter.hpp"


#include <memory>
#include <string>
#include <sstream>
#include "representation/converter/ASCIIRepresentation.hpp"
#include "representation/converter/BinaryRepresentation.hpp"
#include "representation/converter/HexRepresentation.hpp"
#include "../about/AboutDialog.hpp"
#include "../SerialPortProvider.hpp"
#include <logger/LoggerProvider.hpp>


MainWindowPresenter::MainWindowPresenter(MainWindowView &p_view) : m_view(p_view), listen(nullptr) {

	m_view.m_menubar->Bind(wxEVT_COMMAND_MENU_SELECTED, &MainWindowPresenter::on_menu_bar_item_selected, this);

	m_view.Bind(wxEVT_SHOW, &MainWindowPresenter::on_show, this);

	SERIAL_PORT->set_timeout(dstools::SerialPort::NO_TIMEOUT);

	m_about_dialog = new AboutDialog(m_view.GetParent());
	m_view.Bind(wxEVT_CLOSE_WINDOW, [=](wxCloseEvent &event) {
		disconnect();
		event.Skip(true);
		wxDELETE(m_about_dialog);
	});


	m_view.m_connection_view->set_on_connection_button_click_listener(std::bind(&MainWindowPresenter::on_click_connection_button, this, std::placeholders::_1));

	m_basic_view_presenter = std::make_unique<BasicViewPresenter>(*(m_view.m_basic_view));
	m_advanced_view_presenter = std::make_unique<AdvancedViewPresenter>(*(m_view.m_advanced_view));

	m_basic_view_presenter->set_on_send_data_listener([this](const dstools::byte_array &data) {
		m_advanced_view_presenter->add_data(data);
	});

	m_advanced_view_presenter->set_on_send_data_listener([this](const dstools::byte_array &data) {
		m_basic_view_presenter->add_data(data);
	});

	m_basic_view_presenter->set_on_enter_send_mode_listener([this](bool state) {
		m_advanced_view_presenter->set_enter_sends(state);
	});

	m_advanced_view_presenter->set_on_enter_send_mode_listener([this](bool state) {
		m_basic_view_presenter->set_enter_sends(state);
	});
}

void MainWindowPresenter::on_show(wxShowEvent& WXUNUSED(event)) {
	m_view.m_connection_view->init();
	update_componenet_states();
}


void MainWindowPresenter::on_quit() {
	m_view.Close(true);
}

void MainWindowPresenter::on_menu_bar_item_selected(wxCommandEvent &event) {
	if (event.GetId() == wxID_EXIT) {
		event.Skip(true);
		on_quit();
	} else if (event.GetId() == wxID_ABOUT) {
		m_about_dialog->ShowModal();
	}
}


void MainWindowPresenter::on_click_connection_button(sd::ConnectionSettings settings) {
	if (!m_is_connected) {
		connect(std::move(settings));
	} else {
		disconnect();
	}
}

void MainWindowPresenter::update_componenet_states() {
	m_basic_view_presenter->update_connection_status(m_is_connected);
	m_advanced_view_presenter->update_connection_status(m_is_connected);

	m_view.m_connection_view->update_component_states(m_is_connected);
}


void MainWindowPresenter::connect(sd::ConnectionSettings connectionSettings) {
	m_view.m_connection_view->set_enable_of_connection_button(false);
	auto status = std::future_status::ready;

	if (m_connect_disconnect_task.valid()) {
		status = m_connect_disconnect_task.wait_for(std::chrono::milliseconds(0));
	}

	//gui
	if (status == std::future_status::ready) {
		m_connect_disconnect_task = std::async(std::launch::async, [=]() {
			//task
			SERIAL_PORT->set_device(connectionSettings.device);
			SERIAL_PORT->set_speed(connectionSettings.speed);
			SERIAL_PORT->set_parity(connectionSettings.parity);
			SERIAL_PORT->set_stop_bits(connectionSettings.stopbits);
			SERIAL_PORT->set_character_size(connectionSettings.bits);

			bool isFailed = false;
			try {
				SERIAL_PORT->open();
				if (!SERIAL_PORT->is_open()) {
					isFailed = true;
				}
				LOG_INFO({"Connected to device [{}]", connectionSettings.device});
				m_is_connected = true;
				if (listen == nullptr) {
					listen = new std::thread([=]() {
						while (SERIAL_PORT->is_open()) {
							dstools::byte_array data;
							SERIAL_PORT->read(data, 1);
							if (data.empty()) {
								LOG_INFO("No data received");
								continue;
							}
							LOG_INFO("Received " + std::to_string(data.size()) + " bytes");

							m_basic_view_presenter->add_data(data);
							m_advanced_view_presenter->add_data(data);
							update_componenet_states();
						}
					});
				}
			}
			catch (std::runtime_error &e) {
				isFailed = true;
			}

			m_view.GetEventHandler()->CallAfter([=]() {
				//gui
				if (isFailed) {
					wxMessageBox("Cannot connect to device!", "Error!");
				} else {
					m_view.m_connection_view->set_connection_button_label(m_view.m_connection_view->DISCONNECT_LABEL_TEXT);
				}
				m_view.m_connection_view->set_enable_of_connection_button(true);
				update_componenet_states();
			});
		});
	}
}

void MainWindowPresenter::disconnect() {
	auto status = std::future_status::ready;
	m_view.m_connection_view->set_enable_of_connection_button(false);

	if (m_connect_disconnect_task.valid()) {
		status = m_connect_disconnect_task.wait_for(std::chrono::milliseconds(0));
	}

	if (status == std::future_status::ready) {
		m_connect_disconnect_task = std::async(std::launch::async, [=]() {
			if (SERIAL_PORT->is_open()) {
				SERIAL_PORT->close();
				listen->join();
				listen = nullptr;
			}
			m_is_connected = false;
			LOG_INFO("Disconnected from device");
			m_view.GetEventHandler()->CallAfter([=]() {
				m_view.m_connection_view->set_connection_button_label(m_view.m_connection_view->CONNECT_LABEL_TEXT);
				m_view.m_connection_view->set_enable_of_connection_button(true);
				update_componenet_states();
			});
		});
	}
}
