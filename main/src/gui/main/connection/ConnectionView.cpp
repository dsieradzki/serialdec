
#include "ConnectionView.hpp"
#include "../../../core/serial/SerialPortDetectorFactory.hpp"

sd::ConnectionView::ConnectionView(wxWindow *parent, wxWindowID winid, const wxPoint &pos, const wxSize &size,
                                   long style,
                                   const wxString &name) : wxPanel(parent, winid, pos, size, style, name) {

	m_serial_port_detector = ds::createSerialPortDetector();

	auto *root_sizer = new wxStaticBoxSizer(wxVERTICAL, this, "Settings");
	SetSizer(root_sizer);


	auto *settings_row_1_sizer = new wxBoxSizer(wxHORIZONTAL);
	auto *settings_row_2_sizer = new wxBoxSizer(wxHORIZONTAL);

	root_sizer->Add(settings_row_1_sizer, 0, wxEXPAND);
	root_sizer->Add(settings_row_2_sizer, 0, wxEXPAND);



	auto *baud_label = new wxStaticText(this, wxID_ANY, wxT("Speed:"));
	settings_row_1_sizer->Add(baud_label, 0, wxLEFT | wxCENTRE, 5);

	m_speed_combobox = new wxComboBox(this, wxID_ANY);
	m_speed_combobox->SetMaxSize(wxSize(80, 30));
	m_speed_combobox->SetEditable(false);
	m_speed_combobox->Insert("1200", 0);
	m_speed_combobox->Insert("2400", 1);
	m_speed_combobox->Insert("4800", 2);
	m_speed_combobox->Insert("9600", 3);
	m_speed_combobox->Insert("19200", 4);
	m_speed_combobox->Insert("38400", 5);
	m_speed_combobox->Insert("57600", 6);
	m_speed_combobox->Insert("115200", 7);
	m_speed_combobox->Select(3);
	settings_row_1_sizer->Add(m_speed_combobox, 0, wxLEFT | wxCENTRE, 5);


	auto *device_label = new wxStaticText(this, wxID_ANY, wxT("Device:"));
	m_device_combobox = new wxComboBox(this, wxID_ANY, wxT(""));
	m_device_combobox->SetEditable(true);
	settings_row_1_sizer->Add(device_label, 0, wxLEFT | wxCENTRE, 5);
	settings_row_1_sizer->Add(m_device_combobox, 1, wxLEFT | wxCENTRE, 5);


	m_connection_button = new wxButton(this, wxID_ANY, CONNECT_LABEL_TEXT);
	settings_row_1_sizer->Add(m_connection_button, 0, wxLEFT | wxRIGHT | wxCENTER, 5);

	//ROW 2
	auto *bits_label = new wxStaticText(this, wxID_ANY, wxT("Bits:"));
	m_bits_combobox = new wxComboBox(this, wxID_ANY, wxT("8"));
	m_bits_combobox->SetMaxSize(wxSize(50, 30));
	m_bits_combobox->SetEditable(false);
	m_bits_combobox->Insert("7", 0);
	m_bits_combobox->Insert("8", 1);

	settings_row_2_sizer->Add(bits_label, 0, wxLEFT | wxCENTRE, 5);
	settings_row_2_sizer->Add(m_bits_combobox, 0, wxLEFT | wxCENTRE, 5);

	auto *parity_label = new wxStaticText(this, wxID_ANY, wxT("Parity:"));
	m_parity_combobox = new wxComboBox(this, wxID_ANY, wxT("none"));
	m_parity_combobox->SetEditable(false);
	m_parity_combobox->SetMaxSize(wxSize(60, 30));
	m_parity_combobox->Insert("none", 0);
	m_parity_combobox->Insert("odd", 1);
	m_parity_combobox->Insert("even", 2);
	settings_row_2_sizer->Add(parity_label, 0, wxLEFT | wxCENTRE, 5);
	settings_row_2_sizer->Add(m_parity_combobox, 0, wxLEFT | wxCENTRE, 5);

	auto *stopbits_label = new wxStaticText(this, wxID_ANY, wxT("Stop bits:"));
	m_stopbits_combobox = new wxComboBox(this, wxID_ANY, wxT("1"));
	m_stopbits_combobox->SetEditable(false);
	m_stopbits_combobox->SetMaxSize(wxSize(50, 30));
	m_stopbits_combobox->Insert("1", 0);
	m_stopbits_combobox->Insert("2", 1);
	settings_row_2_sizer->Add(stopbits_label, 0, wxLEFT | wxCENTRE, 5);
	settings_row_2_sizer->Add(m_stopbits_combobox, 0, wxLEFT | wxRIGHT | wxCENTRE, 5);


	m_connection_button->Bind(wxEVT_COMMAND_BUTTON_CLICKED, [this](wxCommandEvent& WXUNUSED(event)) {
		if (m_on_connection_button_click_listener) {
			sd::ConnectionSettings connectionSettings;
			connectionSettings.device = m_device_combobox->GetValue().ToStdString();
			connectionSettings.speed = static_cast<unsigned int>(std::stoi(m_speed_combobox->GetValue().ToStdString()));
			connectionSettings.parity = get_parity();
			connectionSettings.stopbits = get_stop_bits();
			connectionSettings.bits = get_character_size();
			m_on_connection_button_click_listener(connectionSettings);
		}
	});

	m_device_combobox->Bind(wxEVT_TEXT, &ConnectionView::on_change_connection_data, this);
	m_device_combobox->Bind(wxEVT_COMBOBOX, &ConnectionView::on_change_connection_data, this);
	m_device_combobox->Bind(wxEVT_COMBOBOX_DROPDOWN, [=](wxCommandEvent& WXUNUSED(event)) { search_serial_ports(); });
	m_speed_combobox->Bind(wxEVT_COMBOBOX, &ConnectionView::on_change_connection_data, this);
	m_speed_combobox->Bind(wxEVT_TEXT, &ConnectionView::on_change_connection_data, this);
}

void sd::ConnectionView::set_on_connection_button_click_listener(std::function<void(sd::ConnectionSettings)> on_connection_button_click_listener) {
	this->m_on_connection_button_click_listener = std::move(on_connection_button_click_listener);
}

void sd::ConnectionView::on_change_connection_data(wxCommandEvent& WXUNUSED(event)) {
	validate_connection_data();
}

void sd::ConnectionView::validate_connection_data() {
	if (m_device_combobox->IsTextEmpty() || m_speed_combobox->IsTextEmpty()) {
		m_connection_button->Enable(false);
	} else {
		m_connection_button->Enable(true);
	}
}

void sd::ConnectionView::search_serial_ports() {
	m_device_combobox->Clear();
	for (std::string portName : m_serial_port_detector->get_list_of_serial_ports()) {
		m_device_combobox->Append(wxString(portName));
	}
}

void sd::ConnectionView::update_component_states(bool is_connected) {
	m_speed_combobox->Enable(!is_connected);
	m_device_combobox->Enable(!is_connected);

	m_bits_combobox->Enable(!is_connected);
	m_parity_combobox->Enable(!is_connected);
	m_stopbits_combobox->Enable(!is_connected);
}

void sd::ConnectionView::set_enable_of_connection_button(bool is_enabled) {
	m_connection_button->Enable(is_enabled);
}

::boost::asio::serial_port_base::parity::type sd::ConnectionView::get_parity() {
	std::string result = m_parity_combobox->GetValue().ToStdString();
	if (result == "none") {
		return ::boost::asio::serial_port_base::parity::type::none;
	} else if (result == "odd") {
		return ::boost::asio::serial_port_base::parity::type::odd;
	} else {
		return ::boost::asio::serial_port_base::parity::type::even;
	}

}

::boost::asio::serial_port_base::stop_bits::type sd::ConnectionView::get_stop_bits() {
	std::string result = m_stopbits_combobox->GetValue().ToStdString();
	if (result == "1") {
		return ::boost::asio::serial_port_base::stop_bits::type::one;
	} else if (result == "2") {
		return ::boost::asio::serial_port_base::stop_bits::type::two;
	} else {
		return ::boost::asio::serial_port_base::stop_bits::type::one;
	}

}

unsigned int sd::ConnectionView::get_character_size() {
	return std::stoul(m_bits_combobox->GetValue().ToStdString());
}

void sd::ConnectionView::set_connection_button_label(wxString label) {
	m_connection_button->SetLabel(label);
}

void sd::ConnectionView::init() {
	search_serial_ports();
	validate_connection_data();
}


