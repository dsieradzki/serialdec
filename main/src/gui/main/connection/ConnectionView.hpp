
#ifndef SERIALDEC_CONNECTIONVIEW_HPP
#define SERIALDEC_CONNECTIONVIEW_HPP

#include <wx/wx.h>
#include <boost/asio.hpp>
#include "../../../core/serial/SerialPortDetector.hpp"

namespace sd {

	struct ConnectionSettings {
		std::string device;
		unsigned int speed;
		unsigned int bits;
		::boost::asio::serial_port_base::parity::type parity;
		::boost::asio::serial_port_base::stop_bits::type stopbits;
	};


	class ConnectionView : public wxPanel {
	public:
		explicit ConnectionView(wxWindow *parent,
		                        wxWindowID winid = wxID_ANY,
		                        const wxPoint &pos = wxDefaultPosition,
		                        const wxSize &size = wxDefaultSize,
		                        long style = wxTAB_TRAVERSAL | wxNO_BORDER,
		                        const wxString &name = wxPanelNameStr);

		void init();
		void set_on_connection_button_click_listener(std::function<void(ConnectionSettings)> on_connection_button_click_listener);

		void update_component_states(bool is_connected);
		void set_enable_of_connection_button(bool is_enabled);
		void set_connection_button_label(wxString label);

		const wxString CONNECT_LABEL_TEXT = "Connect";
		const wxString DISCONNECT_LABEL_TEXT = "Disconnect";
	private:
		std::function<void(ConnectionSettings)> m_on_connection_button_click_listener;
		ds::SerialPortDetector *m_serial_port_detector;

		// Row 1
		wxComboBox *m_speed_combobox;
		wxComboBox *m_device_combobox;
		wxButton *m_connection_button;

		// Row 2
		wxComboBox *m_bits_combobox;
		wxComboBox *m_parity_combobox;
		wxComboBox *m_stopbits_combobox;


		void validate_connection_data();
		void search_serial_ports();

		void on_change_connection_data(wxCommandEvent &event);
		::boost::asio::serial_port_base::parity::type get_parity();
		::boost::asio::serial_port_base::stop_bits::type get_stop_bits();
		unsigned int get_character_size();
	};

}

#endif //SERIALDEC_CONNECTIONVIEW_HPP
