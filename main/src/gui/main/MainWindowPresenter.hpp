
#ifndef SERIALBOX_MAINWINDOWPRESENTER_HPP
#define SERIALBOX_MAINWINDOWPRESENTER_HPP


#include "MainWindowView.hpp"
#include "../../core/serial/SerialPortDetector.hpp"
#include "../about/AboutDialog.hpp"
#include "representation/converter/Representation.hpp"
#include "representation/view/advanced/AdvancedViewPresenter.hpp"
#include <future>
#include <thread>


class MainWindowPresenter {
public:
	explicit MainWindowPresenter(MainWindowView &view);

private:

	void on_menu_bar_item_selected(wxCommandEvent &event);
	void on_click_connection_button(sd::ConnectionSettings settings);

	void on_show(wxShowEvent &event);
	void on_quit();


	void update_componenet_states();
	void connect(sd::ConnectionSettings connectionSettings);
	void disconnect();


	MainWindowView &m_view;

	std::unique_ptr<BasicViewPresenter> m_basic_view_presenter;
	std::unique_ptr<AdvancedViewPresenter> m_advanced_view_presenter;

	bool m_is_connected = false;
	std::thread *listen;

	std::future<void> m_connect_disconnect_task;
	AboutDialog *m_about_dialog;
};


#endif //SERIALBOX_MAINWINDOWPRESENTER_HPP
