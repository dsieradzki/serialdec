
#include <logger/LoggerProvider.hpp>
#include "SerialDeCApplication.hpp"
#include "SerialPortProvider.hpp"


const wxFont SerialDeCApplication::DATA_FONT(wxFontInfo(14).FaceName("Courier"));

bool SerialDeCApplication::OnInit() {
	SERIAL_PORT->set_timeout(dstools::SerialPort::NO_TIMEOUT);

	SetExitOnFrameDelete(true);


	std::string title("SerialDeC ");
	title.append(APPLICATION_VERSION);

	m_main_window_view = new MainWindowView(title);
	m_main_window_view_presenter = new MainWindowPresenter(*m_main_window_view);

	m_main_window_view->Show(true);

	LOG_INFO("Application started");
	return true;
}

int SerialDeCApplication::OnExit() {
	delete m_main_window_view_presenter;
	return 0;
}
