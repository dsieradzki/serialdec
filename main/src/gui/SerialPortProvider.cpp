
#include "SerialPortProvider.hpp"

#include <serial/SerialPortBoost.hpp>


std::shared_ptr<dstools::SerialPort> SERIAL_PORT = std::make_shared<dstools::SerialPortBoost>();
