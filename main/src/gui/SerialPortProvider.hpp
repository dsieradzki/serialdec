
#ifndef SERIALDEC_SERIALPORTPROVIDER_HPP
#define SERIALDEC_SERIALPORTPROVIDER_HPP


#include <memory>
#include <serial/SerialPort.hpp>

extern std::shared_ptr<dstools::SerialPort> SERIAL_PORT;

#endif //SERIALDEC_SERIALPORTPROVIDER_HPP
