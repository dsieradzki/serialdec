
#ifndef SERIALBOX_APPLICATION_HPP
#define SERIALBOX_APPLICATION_HPP

#include <wx/wx.h>
#include "../Version.h"
#include "main/MainWindowView.hpp"
#include "main/MainWindowPresenter.hpp"

class SerialDeCApplication : public wxApp {
public:
	virtual bool OnInit() override;

	int OnExit() override;

	const static wxFont DATA_FONT;
private:
	MainWindowView *m_main_window_view;
	MainWindowPresenter *m_main_window_view_presenter;
};

#endif //SERIALBOX_APPLICATION_HPP
