#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include "../lib/catch.hpp"
#include "../../src/gui/main/representation/converter/Representation.hpp"
#include "../../src/gui/main/representation/converter/HexRepresentation.hpp"
#include "../../src/gui/main/representation/converter/ASCIIRepresentation.hpp"
#include "../../src/gui/main/representation/converter/BinaryRepresentation.hpp"

TEST_CASE("ASCII Conversion", "[ASCIIRepresentation]") {

	Representation *representation = new ASCIIRepresentation();
	std::string input = "data\\nToShow\\nLast";
	std::string expected_with_special = "data\nToShow\nLast";


	// CONVERT TO SEND
	dstools::byte_array data_to_send = representation->convert_to_send(input);
	for (std::size_t i = 0; i < data_to_send.size(); i++) {
		char fir = data_to_send[i];
		char sec = expected_with_special[i];
		REQUIRE(fir == sec);
	}
	REQUIRE(data_to_send.size() == expected_with_special.size());

	// CONVERT INCOMMING
	std::string data_received = representation->parse_ingoing_to_show(data_to_send);

	REQUIRE(expected_with_special == data_received);


	// CONVERT TO SHOW OUTGOING
	std::string to_show = representation->parse_outgoing_to_show(input);

	REQUIRE(to_show == expected_with_special);

}

TEST_CASE("Hex conversion", "[HexRepresentation]") {
	Representation *representation = new HexRepresentation();
	std::string input = "5F";

	// CONVERT TO SEND
	dstools::byte_array data_to_send = representation->convert_to_send(input);
	REQUIRE(data_to_send[0] == 95);

	// CONVERT INCOMMING
	std::string incomming_to_show = representation->parse_ingoing_to_show({(dstools::byte) 95});
	REQUIRE(incomming_to_show == "5F ");

	// CONVERT TO SHOW OUTGOING
	std::string to_show = representation->parse_outgoing_to_show(" afcc  55 0 0");
	REQUIRE( to_show == "AF CC 55 00 ");
}

TEST_CASE("Binary conversion", "[BinaryRepresentation]") {
	Representation *representation = new BinaryRepresentation();
	std::string input = "01010101 10101010 10 10 11 11 11";

	// CONVERT TO SEND
	dstools::byte_array data_to_send = representation->convert_to_send(input);
	REQUIRE(data_to_send[0] == 85);
	REQUIRE(data_to_send[1] == 170);
	REQUIRE(data_to_send[2] == 175);
	REQUIRE(data_to_send[3] == 3);


	// CONVERT INCOMMING
	std::string data_received =representation->parse_ingoing_to_show(data_to_send);
	REQUIRE("01010101 10101010 10101111 00000011 " == data_received);


	// CONVERT TO SHOW OUTGOING
	std::string to_show = representation->parse_outgoing_to_show("01010101 10101010 10 10 11 11 11  ");
	REQUIRE( to_show == "01010101 10101010 10101111 00000011 ");
}


TEST_CASE("Binary To Text conversion", "[BinaryRepresentation]") {
	//asdadsadsa
	//01100001 01110011 01100100 01100001 01100100 01110011 01100001 01100100 01110011 01100001

	Representation *binaryRepresentation = new BinaryRepresentation();
	Representation *textRepresentation = new ASCIIRepresentation();

	std::string input = "01100001 01110011 01100100 01100001 01100100 01110011 01100001 01100100 01110011 01100001";
	std::string out = "asdadsadsa";

	// CONVERT TO SEND
	dstools::byte_array data_to_send = binaryRepresentation->convert_to_send(input);


	// CONVERT INCOMMING
	std::string data_received = textRepresentation->parse_ingoing_to_show(data_to_send);

	REQUIRE(out == data_received);
	
}