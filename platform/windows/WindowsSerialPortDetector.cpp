
#include <tchar.h>
#include <mqoai.h>
#include <main/src/core/logger/LoggerProvider.hpp>

#include "WindowsSerialPortDetector.hpp"

std::vector<std::string> ds::WindowsSerialPortDetector::get_list_of_serial_ports() {
	const int max_length_of_target_path = 10000;
	std::vector<std::string> serial_ports;

	for (int i = 0; i < 255; i++) {

		std::string serial_port_name = "COM" + std::to_string(i);

		TCHAR lp_target_path[max_length_of_target_path];
		DWORD is_exists = QueryDosDevice(reinterpret_cast<LPCSTR>(serial_port_name.c_str()), (LPSTR) lp_target_path, max_length_of_target_path);

		if (is_exists != 0) {
			serial_ports.emplace_back(std::string(serial_port_name));
		}

		if (::GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
			LoggerProvider::get_logger()->error("Find serial ports, buffer too small");
			continue;
		}

	}

	return std::move(serial_ports);
}
