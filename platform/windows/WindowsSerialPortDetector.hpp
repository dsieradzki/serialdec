#ifndef CARWIZARD_WINDOWSSERIALPORTDETECTOR_H
#define CARWIZARD_WINDOWSSERIALPORTDETECTOR_H

#include <vector>
#include "../../main/src/core/serial/SerialPortDetector.hpp"

namespace ds {
	class WindowsSerialPortDetector: public ds::SerialPortDetector {
	public:
		WindowsSerialPortDetector() = default;
		std::vector<std::string> get_list_of_serial_ports() override;

	};

}

#endif //CARWIZARD_WINDOWSSERIALPORTDETECTOR_H
