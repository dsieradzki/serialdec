
#ifndef CARWIZARD_SERIALPORTDETECTORLINUX_HPP
#define CARWIZARD_SERIALPORTDETECTORLINUX_HPP


#include "../../main/src/core/serial/SerialPortDetector.hpp"

namespace ds {
	class SerialPortDetectorLinux : public SerialPortDetector {
	public:
		std::vector<std::string> get_list_of_serial_ports() override;

	private:
		constexpr static const char *SERIAL_PORT_FIND_PATH = "/sys/class/tty/";
		constexpr static const char *DEVICES_PATH = "/dev/";
	};
}
#endif //CARWIZARD_SERIALPORTDETECTORLINUX_HPP
