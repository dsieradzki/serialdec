
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include "SerialPortDetectorLinux.hpp"
#include <boost/algorithm/string/predicate.hpp>

std::vector<std::string> ds::SerialPortDetectorLinux::get_list_of_serial_ports() {
	boost::filesystem::path serial_ports_path(SERIAL_PORT_FIND_PATH);
	boost::filesystem::directory_iterator end_iter;

	if ((!boost::filesystem::exists(serial_ports_path)) || (!boost::filesystem::is_directory(serial_ports_path))) {
		return std::move(std::vector<std::string>());
	}


	std::vector<std::string> result;
	for (boost::filesystem::directory_iterator iter(serial_ports_path); iter != end_iter; ++iter) {

		std::string file_name = iter->path().filename().string();
		std::transform(file_name.begin(), file_name.end(), file_name.begin(), ::tolower);

		if (boost::starts_with(file_name, "ttys") || boost::starts_with(file_name, "ttyusb")) {
			result.emplace_back(DEVICES_PATH + iter->path().filename().string());
		}
	}

	return std::move(result);
}
