
#ifndef SERIALBOX_SERIALPORTDETECTORMACOS_HPP
#define SERIALBOX_SERIALPORTDETECTORMACOS_HPP


#include "../../main/src/core/serial/SerialPortDetector.hpp"

namespace ds {
	class SerialPortDetectorMacos: public ds::SerialPortDetector  {
	public:
		std::vector<std::string> get_list_of_serial_ports() override;
	};
}

#endif //SERIALBOX_SERIALPORTDETECTORMACOS_HPP
