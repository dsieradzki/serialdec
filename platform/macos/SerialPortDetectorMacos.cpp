
#include "SerialPortDetectorMacos.hpp"
#include "cougar/IInterfaces.h"
#include "cougar/InterfacesOSX.h"


std::vector<std::string> ds::SerialPortDetectorMacos::get_list_of_serial_ports() {
	Cougar::Interfaces* interfaces = new Cougar::InterfacesOSX();
	auto devices = interfaces->GetDevices();

	auto result = std::vector<std::string>();
	for (Cougar::SerialDevice device : devices) {
		result.emplace_back(device.calloutDevice);
	}
	return result;
}

